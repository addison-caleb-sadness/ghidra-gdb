package info.addisoncrump.vr.ghidra.gdb.service;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

@UtilityClass
public class GdbUtils {

    @NotNull
    public String escapeExpression(@NotNull String expression) {
        return expression.replaceAll("\\\\", "\\\\").replaceAll("\"", "\\\"");
    }

}
