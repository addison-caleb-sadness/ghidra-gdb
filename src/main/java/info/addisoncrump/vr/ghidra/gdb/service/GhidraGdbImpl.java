package info.addisoncrump.vr.ghidra.gdb.service;

import ghidra.app.util.exporter.BinaryExporter;
import ghidra.app.util.exporter.ExporterException;
import ghidra.framework.plugintool.PluginTool;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Program;
import ghidra.util.Msg;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.jetbrains.annotations.NotNull;
import uk.co.cwspencer.gdb.GdbListenerAdapter;
import uk.co.cwspencer.gdb.messages.GdbEvent;
import uk.co.cwspencer.gdb.messages.GdbStoppedEvent;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.function.Consumer;

import static uk.co.cwspencer.gdb.messages.GdbStoppedEvent.Reason.*;

/**
 * Implementation class of GhidraGdb.
 */
public class GhidraGdbImpl extends GhidraGdb {

    private Program currentProgram;
    private final Path extractedDir;
    private Path outputFile;
    private StreamClosingListener streamClosingListener;
    private LinkedList<FileInputStream> streams;

    /**
     * Constructor; prepares GDB. Use GdbService to acquire an instance.
     *
     * @param gdbPath The path to the GDB executable.
     */
    @SneakyThrows
    protected GhidraGdbImpl(@NotNull Path gdbPath, @NotNull PluginTool tool) {
        super(gdbPath, tool);
        this.extractedDir = Files.createTempDirectory("ghidra-gdb");
        this.streamClosingListener = new StreamClosingListener();
        this.addListener(streamClosingListener);
    }

    @Override
    public synchronized void file(@NotNull Program program, @NotNull Consumer<GdbEvent> cb)
            throws IOException, ExporterException {
        File target = extractedDir.resolve(program.getName()).toFile();
        if (!target.exists()) {
            BinaryExporter exporter = new BinaryExporter();
            if (!exporter.export(target, program,
                    program.getAddressFactory().getAddressSet(), null)) { // monitor isn't used by export
                throw new ExporterException("The program was not successfully exported. Please check the message log for details.");
            }
        }
        this.execute("-file-exec-and-symbols " + target.getAbsolutePath(), cb);
    }

    @Override
    public synchronized void run(@NotNull Consumer<GdbEvent> cb) {
        refreshOutputFile();
        this.sendCommand("run > " + outputFile.toAbsolutePath().toString(), cb);
    }

    @Override
    public synchronized void initialise(@NotNull Consumer<GdbEvent> cb) {
        refreshOutputFile();
        this.sendCommand("starti > " + outputFile.toAbsolutePath().toString(), cb);
    }

    @Override
    public void evaluate(@NotNull String code, @NotNull Consumer<GdbEvent> cb) {
        this.sendCommand(String.format("-data-evaluate-expression \"%s\"", GdbUtils.escapeExpression(code)), cb);
    }

    @Override
    public void invoke(@NotNull Function function, @NotNull Consumer<GdbEvent> cb, @NotNull String... args) {
        this.evaluate(String.format("(%s(*)())%s)(%s)", function.getReturnType().getName(), function.getName(), Arrays.toString(args)), cb);
    }

    @Override
    public void execute(@NotNull String command, @NotNull Consumer<GdbEvent> cb) {
        this.sendCommand(command, cb);
    }

    @SneakyThrows
    @Override
    public synchronized InputStream getProgramOutputStream() {
        FileInputStream stream = new FileInputStream(outputFile.toFile());
        streams.add(stream);
        return stream;
    }

    @SneakyThrows
    @Override
    public synchronized void close() {
        super.close();
        closeAllStreams();
        FileUtils.deleteDirectory(this.extractedDir.toFile());
    }

    @SneakyThrows
    private synchronized void refreshOutputFile() {
        closeAllStreams();
        outputFile = Files.createTempFile(extractedDir, currentProgram.getName(), "-output.log");
    }

    private synchronized void closeAllStreams() {
        streams.removeIf(stream -> {
            try {
                stream.close();
            } catch (IOException e) {
                Msg.showError(GhidraGdbImpl.this, null, "Error While Closing Stream!", "Encountered an error while closing a file stream:", e);
            }
            return true;
        });
    }

    private final class StreamClosingListener extends GdbListenerAdapter {
        @Override
        public void onGdbEventReceived(GdbEvent event) {
            if (event instanceof GdbStoppedEvent) {
                if (Arrays.asList(Exited, ExitedNormally, ExitedSignalled).contains(((GdbStoppedEvent) event).reason)) {
                    closeAllStreams();
                }
            }
        }
    }

}
