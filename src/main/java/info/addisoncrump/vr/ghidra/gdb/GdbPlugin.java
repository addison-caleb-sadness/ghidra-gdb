package info.addisoncrump.vr.ghidra.gdb;

import ghidra.app.plugin.PluginCategoryNames;
import ghidra.app.plugin.ProgramPlugin;
import ghidra.framework.plugintool.PluginInfo;
import ghidra.framework.plugintool.PluginTool;
import ghidra.framework.plugintool.util.PluginStatus;
import info.addisoncrump.vr.ghidra.gdb.service.GdbService;

@PluginInfo(
        status = PluginStatus.STABLE,
        packageName = "Debugger",
        category = PluginCategoryNames.COMMON,
        shortDescription = "GDB Plugin",
        description = "GDB Plugin for Ghidra",
        servicesProvided = {GdbService.class}
)
public class GdbPlugin extends ProgramPlugin {
    // TODO add provider, link gdbservice implementation

    public GdbPlugin(PluginTool tool) {
        super(tool, false, false);
    }
}
