package info.addisoncrump.vr.ghidra.gdb.service;

import ghidra.app.util.exporter.ExporterException;
import ghidra.framework.model.DomainObject;
import ghidra.framework.plugintool.PluginTool;
import ghidra.program.model.address.AddressSetView;
import ghidra.program.model.listing.Function;
import ghidra.program.model.listing.Program;
import ghidra.util.task.TaskMonitor;
import info.addisoncrump.vr.ghidra.gdb.listen.WeakPluggableGdbListener;
import lombok.AccessLevel;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import uk.co.cwspencer.gdb.Gdb;
import uk.co.cwspencer.gdb.GdbListener;
import uk.co.cwspencer.gdb.messages.GdbEvent;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.function.Consumer;

/**
 * Gdb type for Ghidra. Made to be a little more friendly to the Ghidra API.
 */
public abstract class GhidraGdb extends Gdb<WeakPluggableGdbListener> {

    @Getter(AccessLevel.PROTECTED)
    private final PluginTool tool;

    /**
     * Constructor; prepares GDB. Use GdbService to acquire an instance.
     *
     * @param gdbPath The path to the GDB executable.
     * @param tool    The tool used for various interactions with Ghidra.
     */
    protected GhidraGdb(@NotNull Path gdbPath, @NotNull PluginTool tool) {
        super(gdbPath.toAbsolutePath().toString(), null, new WeakPluggableGdbListener());
        this.tool = tool;
    }

    /**
     * Select a program, vis-a-vis the "file" command.
     *
     * @param program The program to select.
     * @return The future containing the completion event.
     * @throws IOException       if there is an IOException thrown during
     *                           {@link ghidra.app.util.exporter.BinaryExporter#export(File, DomainObject, AddressSetView, TaskMonitor) export}.
     * @throws ExporterException if there is an ExporterException thrown during
     *                           {@link ghidra.app.util.exporter.BinaryExporter#export(File, DomainObject, AddressSetView, TaskMonitor) export}.
     */
    public Future<GdbEvent> file(@NotNull Program program)
            throws IOException, ExporterException {
        CompletableFuture<GdbEvent> fev = new CompletableFuture<>();
        this.file(program, fev::complete);
        return fev;
    }

    /**
     * Select a program, vis-a-vis the "file" command.
     *
     * @param program The program to select.
     * @param cb      The callback invoked upon completion.
     * @throws IOException       if there is an IOException thrown during
     *                           {@link ghidra.app.util.exporter.BinaryExporter#export(File, DomainObject, AddressSetView, TaskMonitor) export}.
     * @throws ExporterException if there is an ExporterException thrown during
     *                           {@link ghidra.app.util.exporter.BinaryExporter#export(File, DomainObject, AddressSetView, TaskMonitor) export}.
     */
    public abstract void file(@NotNull Program program, @NotNull Consumer<GdbEvent> cb)
            throws IOException, ExporterException;

    /**
     * Run a program, vis-a-vis the "run" command.
     *
     * @return The future containing the completion event.
     */
    public Future<GdbEvent> run() {
        CompletableFuture<GdbEvent> fev = new CompletableFuture<>();
        this.run(fev::complete);
        return fev;
    }

    /**
     * Run a program, vis-a-vis the "run" command.
     *
     * @param cb The callback invoked upon running the program.
     */
    public abstract void run(@NotNull Consumer<GdbEvent> cb);

    /**
     * Initialise a binary, vis-a-vis the "starti" command. Note that you must use this method for both executable and
     * library binaries in order to use {@link #evaluate(String, Consumer) evaluate} and
     * {@link #invoke(Function, String...) invoke}, an undocumented feature of gdb 8.1+.
     *
     * @param monitor The task monitor to use during the completion of this event.
     * @return The future containing the result of evaluating the expression.
     */
    public Future<GdbEvent> initialise(@Nullable TaskMonitor monitor) {
        CompletableFuture<GdbEvent> fev = new CompletableFuture<>();
        this.initialise(fev::complete);
        return fev;
    }

    /**
     * Initialise a binary, vis-a-vis the "starti" command. Note that you must use this method for both executable and
     * library binaries in order to use {@link #evaluate(String, Consumer) evaluate} and
     * {@link #invoke(Function, String...) invoke}, an undocumented feature of gdb 8.1+.
     *
     * @param cb The callback invoked upon initialising the program.
     */
    public abstract void initialise(@NotNull Consumer<GdbEvent> cb);

    /**
     * Evaluate an expression vis-a-vis the "evaluate" gdb command.
     *
     * @param code The expression to evaluate.
     * @return The future containing the result of evaluating the expression.
     */
    public Future<GdbEvent> evaluate(@NotNull String code) {
        CompletableFuture<GdbEvent> fev = new CompletableFuture<>();
        this.evaluate(code, fev::complete);
        return fev;
    }

    /**
     * Evaluate an expression vis-a-vis the "evaluate" gdb command.
     *
     * @param code The expression to evaluate.
     * @param cb   The callback invoked upon evaluating the expression.
     */
    public abstract void evaluate(@NotNull String code, @NotNull Consumer<GdbEvent> cb);

    /**
     * Invoke a function in the target program.
     *
     * @param function The function to invoke in the target program.
     * @param args     The args to provide to the function.
     * @return The future containing the result of evaluating the expression.
     */
    public Future<GdbEvent> invoke(@NotNull Function function, @NotNull String... args) {
        CompletableFuture<GdbEvent> fev = new CompletableFuture<>();
        this.invoke(function, fev::complete, args);
        return fev;
    }

    /**
     * Invoke a function in the target program.
     *
     * @param function The function to invoke in the target program.
     * @param cb       The callback invoked upon evaluating the expression.
     * @param args     The args to provide to the function.
     */
    public abstract void invoke(@NotNull Function function, @NotNull Consumer<GdbEvent> cb, @NotNull String... args);

    /**
     * Execute an arbitrary GDB command -- for the adventurous among you.
     *
     * @param command The command to invoke on GDB.
     * @return The future containing the result of evaluating the expression.
     */
    public Future<GdbEvent> execute(@NotNull String command) {
        CompletableFuture<GdbEvent> fev = new CompletableFuture<>();
        this.execute(command, fev::complete);
        return fev;
    }

    /**
     * Execute an arbitrary GDB command -- for the adventurous among you. Effectively a wrapper for
     * {@link #sendCommand(String, Consumer) sendCommand}.
     *
     * @param command The command to invoke on GDB.
     * @param cb      The callback invoked upon evaluating the command.
     */
    public abstract void execute(@NotNull String command, @NotNull Consumer<GdbEvent> cb);

    /**
     * Add a listener to the GDB listener set. This is stored in weak references, so keep a reference for as long as you
     * need it!
     *
     * @param listener The listener to add to the listener set.
     */
    public void addListener(GdbListener listener) {
        this.getListener().addListener(listener);
    }

    /**
     * Remove a listener from the GDB listener set. While references are removed when they are collected by GC, this is
     * preferred.
     *
     * @param listener The listener to remove from the listener set. Ensure that it's .equals() or == equivalent.
     */
    public void removeListener(GdbListener listener) {
        this.getListener().removeListener(listener);
    }

    /**
     * Fetches the output stream of the current program. This will be closed when the program terminates.
     *
     * @return The output stream of the current program.
     */
    public abstract InputStream getProgramOutputStream();

}
