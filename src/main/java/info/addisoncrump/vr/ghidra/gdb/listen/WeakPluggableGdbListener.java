package info.addisoncrump.vr.ghidra.gdb.listen;

import ghidra.util.datastruct.WeakDataStructureFactory;
import ghidra.util.datastruct.WeakSet;
import uk.co.cwspencer.gdb.GdbListener;
import uk.co.cwspencer.gdb.gdbmi.GdbMiResultRecord;
import uk.co.cwspencer.gdb.gdbmi.GdbMiStreamRecord;
import uk.co.cwspencer.gdb.messages.GdbEvent;

public class WeakPluggableGdbListener implements GdbListener {

    private final WeakSet<GdbListener> listeners;

    public WeakPluggableGdbListener() {
        listeners = WeakDataStructureFactory.createCopyOnReadWeakSet();
    }

    @Override
    public void onGdbError(Throwable ex) {
        listeners.forEach(listener -> listener.onGdbError(ex));
    }

    @Override
    public void onGdbStarted() {
        listeners.forEach(GdbListener::onGdbStarted);
    }

    @Override
    public void onGdbCommandSent(String command, long token) {
        listeners.forEach(listener -> listener.onGdbCommandSent(command, token));
    }

    @Override
    public void onGdbEventReceived(GdbEvent event) {
        listeners.forEach(listener -> listener.onGdbEventReceived(event));
    }

    @Override
    public void onStreamRecordReceived(GdbMiStreamRecord record) {
        listeners.forEach(listener -> listener.onStreamRecordReceived(record));
    }

    @Override
    public void onResultRecordReceived(GdbMiResultRecord record) {
        listeners.forEach(listener -> listener.onResultRecordReceived(record));
    }

    /**
     * Add a listener to the GDB listener set. This is stored in weak references, so keep a reference for as long as you
     * need it!
     *
     * @param listener The listener to add to the listener set.
     */
    public synchronized void addListener(GdbListener listener) {
        this.listeners.add(listener);
    }

    /**
     * Remove a listener from the GDB listener set. While references are removed when they are collected by GC, this is
     * preferred.
     *
     * @param listener The listener to remove from the listener set. Ensure that it's .equals() or == equivalent.
     */
    public synchronized void removeListener(GdbListener listener) {
        this.listeners.remove(listener);
    }
}
