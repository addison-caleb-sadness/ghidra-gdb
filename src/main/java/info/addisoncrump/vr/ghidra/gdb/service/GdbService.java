package info.addisoncrump.vr.ghidra.gdb.service;

import java.nio.file.Path;

public interface GdbService {

    default GhidraGdb getGdbInstance() {
        return getGdbInstance(true);
    }

    GhidraGdb getGdbInstance(boolean autostart);

    default GhidraGdb getGdbInstance(Path gdbPath) {
        return getGdbInstance(gdbPath, true);
    }

    GhidraGdb getGdbInstance(Path gdbPath, boolean autostart);

}
